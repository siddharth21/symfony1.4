<?php

/**
 * page actions.
 *
 * @package    cms
 * @subpackage page
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pageActions extends sfActions
{

  public function executeView(sfWebRequest $request)
  {
    $page = Doctrine::getTable('sfPage')->findOneBySlug($request->getParameter('slug'));
    
    $this->forward404Unless($page);
    
    $this->page = $page;
  }
}
