<?php

/**
 * main actions.
 *
 * @package    cms
 * @subpackage main
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mainActions extends sfActions
{
    /**
     * Executes index action
     *
     * @param \sfRequest|\sfWebRequest $request A request object
     *
     */
    public function executeIndex(sfWebRequest$request)
    {

    }

    public function executeError404(sfWebRequest$request)
    {
        return sfView::NONE;
    }

}
