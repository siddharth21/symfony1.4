<?php if($menu): ?>
    <ul>
        <?php foreach($menu as $item): ?>
            <li><a href="#"><?php echo $item['title'] ?></a></li>
        <?php endforeach ?>
    </ul>
<?php endif ?>
