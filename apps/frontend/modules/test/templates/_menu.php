<?php if($menu): ?>
    <ul>
        <?php foreach($menu as $item): ?>
            <li><a href="#"><?php echo $item['title'] ?></a></li>
            <?php if($item['__children']): ?>
                <?php include_partial('menu', array('menu' => $item['__children'])) ?>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
<?php endif ?>