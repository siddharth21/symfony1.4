<?php

/**
 * test actions.
 *
 * @package    cms
 * @subpackage test
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class testActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
  
  public function executeMenu1(sfWebRequest $request)
  {
    
    $template = new sfMenuTemplate();
    
    $template->setWrapper('<ul id="left_menu">{{item}}</ul>');
    
    $template->setItemWrapper('<ul>{{item}}</ul>');
    
    $template->setItem('<li><a {{active}} href="{{url}}">{{title}}</a>{{item_wrapper}}</li>');
    
    $template->setActive('style="color: red"');
    
    $this->menu = sfMenuView::init(1)->setTemplate($template)->render('html');
  }
  
  public function executeMenu2(sfWebRequest $request)
  {
    $this->menu = sfMenuView::init(1)->setLevel(1)->render('array');    
  }
  
  public function executeMenu3(sfWebRequest $request)
  {
    $this->menu = sfMenuView::init(1)->render('array');
  }
  
  public function executeCategory(sfWebRequest $request){
    
    $tree = Doctrine_Core::getTable('sfCategory')->getTree();

    $tree = $tree->fetchBranch(2, array(), Doctrine_Core::HYDRATE_ARRAY_HIERARCHY);
    
    $_tree = $this->getHierarchicalSlug($tree);
    
    print_r($_tree);
    
    return sfView::NONE;
  }
  
  public function getHierarchicalSlug($tree, $slug = ''){
      
      foreach($tree as $item){
          
          $_title = sfSeoTransliterator::init()->setString($item['title'])->translit();
          
          if(!empty($slug)){
              
            $_slug = $slug.'/'.$_title;
              
          } else {
           
            $_slug = $_title; 
              
          }
            
          //var_dump($_slug);
              
          if (is_array($item['__children'][0])) {
     
            $_item = $this->getHierarchicalSlug($item['__children'], $_slug);
              
          }
      }
      
  }
  
}
