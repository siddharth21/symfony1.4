<?php

require_once dirname(__FILE__).'/../lib/sfSettingGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sfSettingGeneratorHelper.class.php';

/**
 * sfSetting actions.
 *
 * @package    cms
 * @subpackage sfSetting
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfSettingActions extends autoSfSettingActions
{
}
