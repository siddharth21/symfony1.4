<?php

require_once dirname(__FILE__).'/../lib/sfNewsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sfNewsGeneratorHelper.class.php';

/**
 * sfNews actions.
 *
 * @package    cms
 * @subpackage sfNews
 * @author     GrifiS
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfNewsActions extends autoSfNewsActions
{
}
