<?php

/**
 * sfNews module configuration.
 *
 * @package    cms
 * @subpackage sfNews
 * @author     GrifiS
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfNewsGeneratorConfiguration extends BaseSfNewsGeneratorConfiguration
{
}
