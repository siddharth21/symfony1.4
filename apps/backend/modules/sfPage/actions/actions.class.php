<?php

require_once dirname(__FILE__).'/../lib/sfPageGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sfPageGeneratorHelper.class.php';

/**
 * sfPage actions.
 *
 * @package    cms
 * @subpackage sfPage
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfPageActions extends autoSfPageActions
{
}
