$(document).ready(function(){
  $('#filter_show a').click(function(){
    
    if($('.sf_admin_filter').hasClass('active')){
      
      $('.sf_admin_filter').hide('slow');
    
      $('.sf_admin_filter').removeClass('active');
      
      $(this).text('Показать фильтры');
      
    } else {
      
      $('.sf_admin_filter').show('slow');
    
      $('.sf_admin_filter').addClass('active');
      
      $(this).text('Скрыть фильтры');
      
    }

    return false;
  });
});