<?php

function set_meta_tags($title = null, $description = null, $keywords = null) {

  $response = sfContext::getInstance()->getResponse();

  $main_title = $response->getTitle();

  if (!empty($title)) {

    if (is_array($title)) {

      $title[] = $main_title;

      $new_title = $title;
    } else {

      $new_title = array($title, $main_title);
    }

    $new_title = implode(' | ', $new_title);

    $response->setTitle($new_title);
  }

  if (!empty($description)) {

    $response->addMeta('description', $description);
  }

  if (!empty($keywords)) {

    $response->addMeta('keywords', $keywords);
  }
}