<?php

class Seo extends Doctrine_Template
{

  protected $_options = array();

  public function __construct(array $options = array())
  {
    $this->_options = $options;
  }

  public function setTableDefinition()
  {
      $this->hasColumn('seo_description', 'string', null, array(
           'type' => 'text'
           ));

      $this->hasColumn('seo_keywords', 'string', 255, array(
           'type' => 'string',
           'length' => 255
           ));

  }

}