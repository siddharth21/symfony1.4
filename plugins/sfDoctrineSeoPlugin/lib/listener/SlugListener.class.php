<?php


/**
 * Easily hash creator
 */
class SlugListener extends Doctrine_Record_Listener
{
  /**
   * Array of sortable options
   *
   * @var array
   */
  protected $_options = array();


  /**
   * __construct
   *
   * @param array $options
   * @return void
   */
  public function __construct(array $options)
  {
    $this->_options = $options;
  }


  /**
   * Set hash when record created
   *
   * @param Doctrine_Event $event
   * @return void
   */
  public function postSave(Doctrine_Event $event)
  {
    
    $object = $event->getInvoker();

    if(!$object->slug){

      $i = 0;

      $prefix = '';

      $_slug = '';

      foreach($this->_options['fields'] as $val){

        if($i > 0) $prefix = '-';

        $_slug .= $prefix.trim($object->$val);

        $i++;
      }

      $slug = strtolower($this->translitIt($_slug));

      if(isset($this->_options['time'])) $slug .= '-'.time();

      $object->slug = $slug;

      $object->save();

    }

    return parent::postSave($event);
  }
  

  protected function translitIt($str)
  {
      $tr = array(
          "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
          "Д"=>"d","Е"=>"e",'Ё'=>'yo',"Ж"=>"zh","З"=>"z","И"=>"i",
          "Й"=>"j","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
          "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
          "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"c","Ч"=>"ch",
          "Ш"=>"sh","Щ"=>"shh","Ъ"=>"","Ы"=>"y","Ь"=>"",
          "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
          "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"zh",
          "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
          "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
          "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
          "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
          "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
          " "=> "-"
      );
      
      $str = strtr($str, $tr);

      $str = preg_replace('/[^0-9a-zA-Z-]/', '',$str);

      return $str;
  }

}