<?php

class sfSeoTransliterator {
    
    protected $separator = '-';
    
    protected $string;
    
    static public function init(){
        
        return new sfSeoTransliterator;
    }
    
    public function setSeparator($separator){
        
        $this->separator = $separator;
        
        return $this;
    }
    
    public function setString($string){
        
        $this->string = $string;
        
        return $this;
    }
    
    protected function process()
    {
          $tr = array(
              "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
              "Д"=>"d","Е"=>"e",'Ё'=>'yo',"Ж"=>"zh","З"=>"z","И"=>"i",
              "Й"=>"j","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
              "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
              "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"c","Ч"=>"ch",
              "Ш"=>"sh","Щ"=>"shh","Ъ"=>"","Ы"=>"y","Ь"=>"",
              "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
              "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"zh",
              "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
              "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
              "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
              "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
              "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
              " "=> "-"
          );
          
          $str = strtr($this->string, $tr);
    
          $str = preg_replace('/[^0-9a-zA-Z-]/', '',$str);
    
          return $str;
    }
    
    public function translit(){
        
        $str = $this->process();
        
        return $str;
    }
    
}
