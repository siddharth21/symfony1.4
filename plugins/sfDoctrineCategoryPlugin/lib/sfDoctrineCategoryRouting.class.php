<?php

class sfDoctrineCategoryRouting extends sfPatternRouting {

  static public function listenToRoutingLoadConfigurationEvent(sfEvent $event) {

    $routing = $event->getSubject();

    $routing->prependRoute('sf_category', new sfDoctrineRouteCollection(array(
      'name'                => 'sf_category',
      'model'               => 'sfCategory',
      'module'              => 'sfCategory',
      'prefix_path'         => 'sf_category',
      'with_wildcard_routes' => true,
      'requirements'        => array()
    )));
    
    $routing->prependRoute('sf_category_items', new sfRoute('sf_category/:root_id/items', array('module' => 'sfCategoryItem', 'action' => 'index'))); 
  }

}
