<?php


/**
 * Category slug creator
 */
class CategorySlugListener extends Doctrine_Record_Listener
{
  /**
   * Array of sortable options
   *
   * @var array
   */
  protected $_options = array();


  /**
   * __construct
   *
   * @param array $options
   * @return void
   */
  public function __construct(array $options)
  {
    $this->_options = $options;
  }


  /**
   * Set hash when record created
   *
   * @param Doctrine_Event $event
   * @return void
   */
  public function postSave(Doctrine_Event $event)
  {
      $object = $event->getInvoker();
      
      $_object = Doctrine_Core::getTable('sfCategory')->find($object->getId());
      
      $level = $_object->getLevel();
      
      if($_object->getNode()->hasParent()){
          
        if($level > 0){
            
            for ($i=1; $i < $level; $i++) {
                 
              $_object = $_object->getNode()->getParent();
                
            }
        }
            
      }
      
      $tree = Doctrine_Core::getTable('sfCategory')->getTree();

      $tree = $tree->fetchBranch($_object->getId(), array(), Doctrine_Core::HYDRATE_ARRAY_HIERARCHY);
    
      $_tree = $this->setHierarchicalSlug($tree);
      
      return parent::postSave($event);
      
  }
  
  public function setHierarchicalSlug($tree, $slug = ''){
      
      foreach($tree as $item){
          
          $_title = sfSeoTransliterator::init()->setString($item['title'])->translit();
          
          if(!empty($slug)){
              
            $_slug = $slug.'/'.$_title;
              
          } else {
           
            $_slug = $_title; 
              
          }
          
          Doctrine_Query::create()
                ->update('sfCategory c')
                ->set('c.slug', '?', $_slug)
                ->where('c.id = ?', $item['id'])
                ->execute();
              
          if (is_array($item['__children'])) {
     
            $_item = $this->setHierarchicalSlug($item['__children'], $_slug);
              
          }
      }   
  }
  
}