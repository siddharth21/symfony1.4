<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * (c) Jonathan H. Wage <jonwage@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfWidgetFormDoctrineCategoryChoice represents a choice widget for a model.
 *
 * @package    symfony
 * @subpackage doctrine
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @author     Jonathan H. Wage <jonwage@gmail.com>
 * @version    SVN: $Id: sfWidgetFormDoctrineChoice.class.php 29679 2010-05-30 14:46:03Z Kris.Wallsmith $
 */
class sfWidgetFormDoctrineCategoryChoice extends sfWidgetFormChoice {

  /**
   * @see sfWidget
   */
  public function __construct($options = array(), $attributes = array()) {
    $options['choices'] = array();

    parent::__construct($options, $attributes);
  }

  /**
   * Constructor.
   *
   * Available options:
   *
   *  * root_id:      The root_id (required)
   *  * add_empty:    Whether to add a first empty value or not (false by default)
   *                  If the option is not a Boolean, the value will be used as the text value
   *  * method:       The method to use to display object values (__toString by default)
   *  * key_method:   The method to use to display the object keys (getPrimaryKey by default)
   *  * 
   *  * query:        Custom query
   *
   * @see sfWidgetFormSelect
   */
  protected function configure($options = array(), $attributes = array()) {
    $this->addRequiredOption('root_id');
    $this->addOption('add_empty', false);
    $this->addOption('method', '__toString');
    $this->addOption('key_method', 'getPrimaryKey');
    $this->addOption('multiple', false);
    $this->addOption('query', false);

    parent::configure($options, $attributes);
  }

  /**
   * Returns the choices associated to the model.
   *
   * @return array An array of choices
   */
  public function getChoices() {
    $choices = array();
    if (false !== $this->getOption('add_empty')) {
      $choices[''] = true === $this->getOption('add_empty') ? '' : $this->translate($this->getOption('add_empty'));
    }

    $query = $this->getOption('query') ? $this->getOption('query') : Doctrine_Core::getTable('sfCategory')->getChildCategories($this->getOption('root_id'));
    
    $objects = $query->execute();

    $method = $this->getOption('method');
    $keyMethod = $this->getOption('key_method');

    foreach ($objects as $object) {
      $choices[$object->$keyMethod()] = $object->$method();
    }

    return $choices;
  }

}