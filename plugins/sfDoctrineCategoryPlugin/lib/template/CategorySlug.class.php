<?php

class CategorySlug extends Doctrine_Template
{

  protected $_options = array();

  public function __construct(array $options = array())
  {
    $this->_options = $options;
  }

  public function setTableDefinition()
  {
    $this->hasColumn('slug', 'string', 255, array(
           'type' => 'string',
           'length' => 255
           ));

    $this->addListener(new CategorySlugListener($this->_options));
    
  }

}