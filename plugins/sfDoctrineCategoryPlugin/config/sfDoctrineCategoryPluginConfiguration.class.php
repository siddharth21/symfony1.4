<?php

/**
 * sfDoctrineCategoryPlugin configuration.
 * 
 * @package     sfDoctrineMenuPlugin
 * @subpackage  config
 * @author      Greg.Schurgast
 * @version     SVN: $Id: PluginConfiguration.class.php 17207 2009-04-10 15:36:26Z Kris.Wallsmith $
 */
class sfDoctrineCategoryPluginConfiguration extends sfPluginConfiguration
{
  const VERSION = '1.0';

  /**
   * @see sfPluginConfiguration
   */
  public function configure()
  {
    $this->dispatcher->connect('routing.load_configuration', array('sfDoctrineCategoryRouting', 'listenToRoutingLoadConfigurationEvent'));
  }
}
