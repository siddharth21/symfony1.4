<?php
/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once(dirname(__FILE__).'/../lib/BaseSfCategoryItemComponents.class.php');

/**
 * sfDoctrineCategoryItem components.
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Gregory Schurgast <michkinn@gmail.com>
 * @author     Gordon Franke <info@nevalon.de>
 * @version    SVN: $Id: sfDoctrineCategoryItemComponents.class.php 7745 2008-03-05 11:05:33Z michkinn $
 */

class sfCategoryItemComponents extends BaseSfCategoryItemComponents
{
}