<?php use_stylesheet('/sfDoctrineCategoryPlugin/js/themes/default/style.css') ?>
<?php use_stylesheet('/sfDoctrineCategoryPlugin/js/fancybox/jquery.fancybox-1.3.4.css') ?>
<?php use_stylesheet('/sfDoctrineCategoryPlugin/css/menu.css') ?>
<?php use_javascript('/sfDoctrineCategoryPlugin/js/jquery.jstree.js') ?>
<?php use_javascript('/sfDoctrineCategoryPlugin/js/fancybox/jquery.fancybox-1.3.4.pack.js') ?>
<?php use_javascript('/sfDoctrineCategoryPlugin/js/jquery.form.js') ?>
<?php use_javascript('/sfDoctrineCategoryPlugin/js/jquery.validate.min.js') ?>
<?php use_javascript('/sfDoctrineCategoryPlugin/js/category_item.js') ?>
<div id="sf_admin_container">
  <div><h1><?php echo $title ?></h1></div>
  <div id="tree">
    <?php echo html_entity_decode($items); ?>
  </div>

  <div style="margin-top: 15px"><input id="create_item" type="button" value="Создать новую категорию" /></div>
</div>
<script type="text/javascript">
  
  var parent;
  
  var root = <?php echo $root ?>;
  
  var url_get_form = "<?php echo url_for('sfCategoryItem/getForm') ?>";
  
  var url_create = "<?php echo url_for('sfCategoryItem/create') ?>";
  
  var url_edit = "<?php echo url_for('sfCategoryItem/edit') ?>";
  
  var url_move = "<?php echo url_for('sfCategoryItem/move') ?>";
  
  var url_remove = "<?php echo url_for('sfCategoryItem/remove') ?>";

</script>