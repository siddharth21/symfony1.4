<script type="text/javascript">
  $(function () { 
    $("#item_form").validate({
      rules: {
        "sf_category[title]": "required"
      },
      messages: {
        "sf_category[title]": "Обязательно"
      }
    });
  });
</script>
<div class="ajax_form">
  <form id="item_form" action="<?php echo url_for('sfCategoryItem/edit') ?>" method="post" >
    <?php foreach ($form as $field): ?>
      <?php if (!$field->isHidden()): ?>
        <div class="label"><?php echo $field->renderLabel() ?></div>
        <div class="field"><?php echo $field->render() ?></div>
      <?php endif ?>
    <?php endforeach ?>
    <div class="hidden_fields"><?php echo $form->renderHiddenFields() ?></div>
    <div class="submit">
      <input name="save" id="item_submit" type="submit" value="Сохранить" /><img id="form_loader" style="display: none" src="/sfDoctrineCategoryPlugin/images/loader.gif" />
    </div>
  </form>
</div>