<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfJqueryTreeDoctrineManager actions.
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Gregory Schurgast <michkinn@gmail.com>
 * @author     Gordon Franke <info@nevalon.de>
 * @version    SVN: $Id: BasesfGuardForgotPasswordActions.class.php 18401 2009-05-18 14:12:09Z gimler $
 */
class BaseSfCategoryItemActions extends sfActions {

  protected $model = 'sfCategory';

  public function executeEdit($request) {

    $out = array();

    $raw_value = $request->getParameter('sf_category');
    
    if($raw_value['id']){
      
      $form = new sfCategoryForm(Doctrine::getTable($this->model)->find($raw_value['id']));
      
    } else {
      
      $form = new sfCategoryForm();
      
    }

    $form->bind($raw_value);

    if ($form->isValid()) {

      if ($form->isNew()) {

        $parent_id = $raw_value['parent'];

        $parent = Doctrine_Core::getTable($this->model)->findOneById($parent_id);

        $form->save();

        $form->getObject()->getNode()->moveAsLastChildOf($parent);
        
        $out['parent'] = $parent_id;
        
        $out['action'] = 'create';
        
      } else {

        $form->save();
        
        $out['action'] = 'edit';
        
      }

      $out['data'] = $form->getObject()->toArray();

      $out['success'] = 1;

      $out['id'] = $form->getObject()->getId();

      $out['title'] = $form->getObject()->getTitle();
      
      

    } else {

      $out['error'] = 'error';
      
      $out['errors'] = $form->getErrorSchema()->getMessage();
    }

    $this->getResponse()->setContent(json_encode($out));

    return sfView::NONE;
  }

  public function executeRemove($request) {
    $id = $request->getParameter('item_id');

    $record = Doctrine_Core::getTable($this->model)->find($id);
    $record->getNode()->delete();

    $out['success'] = 1;

    $out['id'] = $record->getId();

    $this->getResponse()->setContent(json_encode($out));

    return sfView::NONE;
  }

  public function executeMove($request) {

    $id = $request->getParameter('id');
    $ref_id = $request->getParameter('ref_id');
    $type = $request->getParameter('type');

    $record = Doctrine_Core::getTable($this->model)->find($id);
    $dest = Doctrine_Core::getTable($this->model)->find($ref_id);

    if ($type == 'last') {
      $record->getNode()->moveAsLastChildOf($dest);
    } else if ($type == 'after') {
      $record->getNode()->moveAsNextSiblingOf($dest);
    } else if ($type == 'before') {
      $record->getNode()->moveAsPrevSiblingOf($dest);
    }

    $out = array();

    $this->getResponse()->setContent(json_encode($out));

    return sfView::NONE;
  }

  public function executeGetForm(sfWebRequest $request) {
    
    $item_id = $request->getParameter('item');
    
    if($item_id){
      
      $item = Doctrine::getTable($this->model)->find($item_id);
      
      $form = new sfCategoryForm($item);
      
    } else {
      
      $form = new sfCategoryForm();
      
    }

    $this->form = $form;

    $this->setLayout(false);
  }

  public function executeIndex(sfWebRequest $request) {

    if (!Doctrine_Core::getTable($this->model)->isTree()) {
      throw new Exception('Model "' . $this->model . '" is not a NestedSet');
      return false;
    }

    $this->root = $request->getParameter('root_id');

    $tree = Doctrine_Core::getTable($this->model)->getTree();

    $tree = $tree->fetchTree(array('root_id' => $this->root), Doctrine_Core::HYDRATE_ARRAY_HIERARCHY);
    
    $this->title = $tree[0]['title'];
    
    $tree = $tree[0]['__children'];
    
    $this->items = $this->genHierarchicalCategory($tree, 'title');
  }

  protected function genHierarchicalCategory($tree, $field) {

    $out = '<ul class="nested_set_list">' . "\n";

    $out .= $this->getTree($tree, $field);

    $out .= '</ul>' . "\n";

    return $out;
  }

  protected function getTree($records, $field) {

    $out .= '';

    foreach ($records as $key => $record) {

      $out .= '<li title="' . $record[$field] . '" id="phtml_' . $record['id'] . '" item_id="' . $record['id'] . '">' . "\n";

      $out .= '<a href="#">' . $record[$field] . '</a>' . "\n";

      if (is_array($record['__children'][0])) {

        $out .= '<ul>' . "\n";

        $out .= $this->getTree($record['__children'], $field);

        $out .= '</ul>' . "\n";
      }

      $out .= '</li>' . "\n";
    }

    return $out;
  }

}
