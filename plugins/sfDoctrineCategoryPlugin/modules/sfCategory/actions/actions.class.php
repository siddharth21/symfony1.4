<?php

require_once dirname(__FILE__).'/../lib/sfCategoryGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sfCategoryGeneratorHelper.class.php';

/**
 * Category actions.
 *
 * @package    cms
 * @subpackage Category
 * @author     GrifiS
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfCategoryActions extends autoSfCategoryActions
{
  
  public function executeListItems(sfWebRequest $request)
  {
    $this->redirect('@sf_category_items?root_id='.$this->getRoute()->getObject()->getId());
  }
  
  protected function buildQuery() {

    $query = parent::buildQuery();

    $query->andWhere('level = ?', 0);

    return $query;
    
  }
  
}
