<?php
/**
 * sfGenerateComponentPlugin configuration.
 * 
 * @package     sfGenerateComponentPlugin
 * @subpackage  task
 * @author      hidenorigoto
 */
class generateComponentTask extends sfBaseTask
{
  protected function configure()
  {
      $this->addArguments(array(
        new sfCommandArgument('application', sfCommandArgument::REQUIRED, 'The application name'),
        new sfCommandArgument('module', sfCommandArgument::REQUIRED, 'The module name'),
      ));

    $this->namespace        = 'generate';
    $this->name             = 'component';
    $this->briefDescription = 'Generates a new component';
    $this->detailedDescription = <<<EOF
The [generate:component|INFO] task does things.
Call it with:

  [./symfony generate:component frontend article|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
      $app    = $arguments['application'];
      $module = $arguments['module'];

      // Validate the module name
      if (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $module))
      {
          throw new sfCommandException(sprintf('The module name "%s" is invalid.', $module));
      }

      $moduleDir = sfConfig::get('sf_app_module_dir').'/'.$module;

      $properties = parse_ini_file(sfConfig::get('sf_config_dir').'/properties.ini', true);

      $constants = array(
        'PROJECT_NAME' => isset($properties['symfony']['name']) ? $properties['symfony']['name'] : 'symfony',
        'APP_NAME'     => $app,
        'MODULE_NAME'  => $module,
        'AUTHOR_NAME'  => isset($properties['symfony']['author']) ? $properties['symfony']['author'] : 'Your name here',
      );

      $skeletonDir = dirname(__FILE__).'/skeleton';

      // create basic application structure
      $finder = sfFinder::type('any')->discard('.sf');
      $this->getFilesystem()->mirror($skeletonDir, $moduleDir, $finder);

      // customize php and yml files
      $finder = sfFinder::type('file')->name('*.php', '*.yml');
      $this->getFilesystem()->replaceTokens($finder->in($moduleDir), '##', '##', $constants);
  }
}
