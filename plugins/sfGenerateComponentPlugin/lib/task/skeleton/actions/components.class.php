<?php
/**
 * ##MODULE_NAME## components.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage ##MODULE_NAME##
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: components.class.php $
 */
class ##MODULE_NAME##Components extends sfComponents
{
 /**
  * sample component
  */
  public function executeSample()
  {
  }
}
