<?php

/*
 * This file is part of the isicsWidgetFormTinyMCEPlugin package.
 * Copyright (c) 2008 ISICS.fr <contact@isics.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * TinyMCE Widget
 *
 * @package sfWidgetFormTinymce
 * @author GrifiS
 * */
class sfWidgetFormTinymce extends sfWidgetFormTextarea {

  /**
   * Constructor.
   *
   * Available options:
   *  * format:   TinyMCE options
   *
   * @see sfWidgetFormTextarea
   * */
  protected function configure($options = array(), $attributes = array()) {
    $this->addOption('format', 'default');
  }

  /**
   * @param  string $name        The element name
   * @param  string $value       The value displayed in this widget
   * @param  array  $attributes  An array of HTML attributes to be merged with the default HTML attributes
   * @param  array  $errors      An array of errors for the field
   *
   * @see sfWidget
   * */
  public function render($name, $value = null, $attributes = array(), $errors = array()) {

    $id = $this->generateId($name, $value);

    $format = sfConfig::get('tinymce_' . $this->getOption('format'));

    $i = count($format);

    $options = 'script_url: "/sfTinymcePlugin/js/tinymce/tiny_mce.js",'."\n";

    foreach ($format as $key => $val) {

      $options .= !is_bool($val) ? $key . ': "' . $val . '"' : $key . ': ' . intval($val);
      
      $options .= $i-- != 1 ? ',' : '';

      $options .= "\n";
    }

    $javascript = <<<JS
  <script type="text/javascript">
  $(function() {
    
    $('#{$id}').tinymce(
      {
        {$options}
      }
    );
  });
  </script>
JS;


    return $javascript . "\n" . parent::render($name, $value, $attributes, $errors);
  }

  /**
   * Gets the JavaScript paths associated with the widget.
   *
   * @return array An array of JavaScript paths
   */
  public function getJavascripts() {
    return array(
        '/sfTinymcePlugin/js/tinymce/jquery.tinymce.js'
    );
  }

}