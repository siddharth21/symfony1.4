<?php

class sfImageCachePluginConfiguration extends sfPluginConfiguration {
  const VERSION = '0.1';

  /**
   * @see sfPluginConfiguration
   */
  public function initialize() {
    if ($this->configuration instanceof sfApplicationConfiguration) {
      require_once($this->configuration->getConfigCache()->checkConfig('config/imagecache.yml'));
    }
  }

}
