<?php

function image_cache_url($route, $format, $path, $image, $alt = null) {

  $_formats = sfConfig::get('imagecache_formats');

  $_format = $_formats[$format];

  if (!isset($_format)) {

    throw new RuntimeException('Format not found');
  }

  $real_file_path = sfConfig::get('sf_upload_dir'). '/' . $path . '/' . $image;

  if (file_exists($real_file_path)) {
    
    $options = array('path' => $format.'/'.$path.'/'.$image);
    
    $url = urldecode(url_for($route, $options));

    return $url;
    
  } else {

    return '';
  }
  
}

function image_cache_tag($route, $format, $path, $image, $alt = null) {

  $_formats = sfConfig::get('imagecache_formats');

  $_format = $_formats[$format];

  if (!isset($_format)) {

    throw new RuntimeException('Format not found');
  }

  $real_file_path = sfConfig::get('sf_upload_dir') . '/' . $path . '/' . $image;

  if (file_exists($real_file_path)) {
    
    $cache_file_path = sfConfig::get('sf_upload_dir') . '/cache/' . $format . '/' . $path . '/' . $image;
    
    $is_cached_file = file_exists($cache_file_path);

    $options = array('path' => $format.'/'.$path.'/'.$image);
    
    $url = urldecode(url_for($route, $options));

    if ($is_cached_file) {

      $cached_image = new sfImage($cache_file_path);

      $width = $cached_image->getWidth();

      $height = $cached_image->getHeight();

      return image_tag($url, array('size' => $width . 'x' . $height, 'alt' => $alt));
      
    } else {

      return image_tag($url, array('alt' => $alt));
      
    }
    
  } else {

    return '';
  }
}