<?php

class ImageCache extends Doctrine_Template {

  protected $_options = array();

  public function __construct(array $options = array()) {
    $this->_options = $options;
  }

  public function setTableDefinition() {

    $table = $this->getTable();

    $this->addListener(new ImageCacheListener($this->_options, $table));
  }

}