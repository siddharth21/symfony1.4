<?php

/**
 * Description of sfImageCache
 *
 * @author grifis
 */
class sfImageCache {

  protected $parent_image;
  protected $image;
  protected $actions;
  protected $format;
  protected $format_name;
  protected $path;

  static public function init($_format, $_path, $_image) {

    return new sfImageCache($_format, $_path, $_image);
  }

  public function __construct($_format, $_path, $_image) {

    $this->format_name = $_format;

    $this->path = $_path;

    $_formats = sfConfig::get('imagecache_formats');

    $format = $_formats[$_format];

    if (!isset($format)) {

      throw new RuntimeException('Format not found');
    }

    $this->format = $format;

    $this->registerActions();

    $this->checkActions();

    $image_path = sfConfig::get('sf_upload_dir') . '/' . $_path . '/' . $_image;

    if (!file_exists($image_path)) {

      throw new RuntimeException('Image not found');
    }

    $this->parent_image = $_image;

    $this->image = new sfImage($image_path);
  }

  protected function registerActions() {

    $this->actions = array(
        'resize',
        'crop',
        'overlay'
    );
  }

  protected function getRegisteredActions() {

    return $this->actions;
  }

  protected function checkActions() {

    if (empty($this->format['actions'])) {

      throw new RuntimeException('You must set format actions.');
    }

    foreach ($this->format['actions'] as $action) {

      if (!in_array($action['name'], $this->getRegisteredActions())) {

        throw new RuntimeException('Action "' . $action['name'] . '" not registered');
      }
    }
  }

  public function generate() {

    $this->setQuality($this->format['quality']);

    foreach ($this->format['actions'] as $action) {

      $this->$action['name']($action);
    }

    return $this;
  }

  public function setQuality($quality) {

    if (!$quality)
      $quality = 90;

    $this->image->setQuality($quality);

    return $this;
  }

  public function resize($action) {

    $this->image->resize($action['width'], $action['hight'], true, true);

    return $this;
  }

  public function crop($action) {

    $this->image->resize($action['left'], $action['top'], $action['width'], $action['hight']);

    return $this;
  }

  public function overlay($action) {

    if (!isset($action['image'])) {

      throw new RuntimeException('Overlay image not set');
    }

    $this->image->overlay(new sfImage($action['image']), $action['position']);

    return $this;
  }

  public function save($name) {

    if (!isset($name)) {

      throw new RuntimeException('New image name not set');
    }

    $name = trim($name);

    $dir = sfConfig::get('sf_upload_dir') . '/cache/' . trim($this->format_name);

    if (!is_dir($dir)) {

      try {
        
        mkdir($dir, 0777);
      
      } catch (Exception $exc) {

        throw new RuntimeException('New dir not created');
      }
      
    }

    $dir = $dir.'/'.$this->path;

    if (!is_dir($dir)) {

      try {

        mkdir($dir, 0777, true);

      } catch (Exception $exc) {

        throw new RuntimeException('New dir not created');
      }

    }

    $this->image->saveAs($dir . '/' . $name);

    return $this;
  }

  public function getImage() {

    return $this->image;
  }

}

?>
