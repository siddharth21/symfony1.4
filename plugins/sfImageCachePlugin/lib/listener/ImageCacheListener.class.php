<?php

/**
 * Deleted image and image cache
 */
class ImageCacheListener extends Doctrine_Record_Listener
{

  /**
   * Array of sortable options
   *
   * @var array
   */
  protected $_options = array();

  /**
   * __construct
   *
   * @param array $options
   * @return void
   */
  public function __construct(array $options, $table)
  {
    $this->_options = $options;

    $this->_table = $table;
  }

  /**
   * Set hash when record created
   *
   * @param Doctrine_Event $event
   * @return void
   */
  public function postDelete(Doctrine_Event $event)
  {
    parent::postDelete($event);

    $object = $event->getInvoker();

    $_formats = sfConfig::get('imagecache_formats');

    $columns = $this->_table->getColumns();

    foreach ($columns as $column_name => $def) {

      if ($extra = $def['extra']) {

        if ($extra['type'] == 'image') {

          $value = $object->get($column_name);

          if (!empty($value)) {

            $_format = $_formats[$extra['imageCacheFormat']];

            $file = sfConfig::get('sf_upload_dir') . '/' . trim($extra['path']) . '/' . $value;

            unlink($file);

            foreach ($_formats as $format => $options) {

              $cached_file = sfConfig::get('sf_upload_dir') . '/cache/' . trim($format) . '/' . $extra['path'] . '/' . $value;

              unlink($cached_file);
            }

          }

          unset($value);
        }
      }
    }
  }

}