<?php

class sfImageCacheActions extends sfActions {

  public function executeIndex(sfWebRequest $request) {

    $path = urldecode($request->getParameter('path'));

    $path_a = explode('/', $path);

    $path_count = count($path_a);

    $format = trim($path_a[0]);

    $image = trim($path_a[$path_count-1]);

    unset($path_a[0], $path_a[$path_count-1]);

    $path = implode('/', $path_a);

    $image_cache = new sfImageCache($format, $path, $image);
    
    $image_cache->generate();
    
    $image_cache->save($image);
    
    $_image = $image_cache->getImage();
    
    $response = $this->getResponse();
    $response->setContentType($_image->getMIMEType());
    $response->setContent($_image->toString());
    
    return sfView::NONE;
  }
  
  

}