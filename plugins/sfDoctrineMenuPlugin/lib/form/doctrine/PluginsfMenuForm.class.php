<?php

/**
 * PluginsfMenu form.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: sfDoctrineFormPluginTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class PluginsfMenuForm extends BasesfMenuForm
{
  public function configure() {
    
    $this->getWidget('permissions_list')->setOption('expanded', true);
    
    $this->setWidget('parent', new sfWidgetFormInputHidden());
    
    $this->setValidator('parent', new sfValidatorInteger(array('required' => false)));
    
    $this->validatorSchema->setOption('allow_extra_fields', true);
    $this->validatorSchema->setOption('filter_extra_fields', false);
    
    unset($this['root_id'], $this['lft'], $this['rgt'], $this['level']);
    
  }
}
