<?php

class sfMenuView {

  private $path_prefix;
  private $current_path;
  private $template;
  private $item_id;
  private $level = false;
  private $formats = array('html', 'json', 'array');
  private $format = 'array';
  
  private $absolute_url = false;

  static public function init($item_id) {

    return new sfMenuView($item_id);
  }

  public function __construct($item_id) {

    $this->setDefaulTemplate();

    $this->item_id = $item_id;
    
    $this->path_prefix = sfContext::getInstance()->getRequest()->getPathInfoPrefix();
    
    $this->current_path = sfContext::getInstance()->getRequest()->getPathInfo();
    
  }
  
  public function setDefaulTemplate(){
    
    $this->template = new sfMenuTemplate();

    $this->template->setWrapper('<ul id="menu">{{item}}</ul>');

    $this->template->setItemWrapper('<ul>{{item}}</ul>');

    $this->template->setItem('<li class="{{active}}"><a href="{{url}}" {{target}}>{{title}}</a>{{item_wrapper}}</li>');
    
    return $this;
    
  }

  public function setTemplate(sfMenuTemplate $template) {

    $this->template = $template;
    
    return $this;
  }

  public function setLevel($level = 1) {

    $this->level = $level;
    
    return $this;
  }
  
  public function setAbsoluteUrl($absolute) {

    $this->absolute_url = $absolute;
    
    return $this;
  }

  protected function getMenu() {
    
    $q = Doctrine_Query::create()
    ->select('m.title, m.url, m.is_new_window, p.name')
    ->from('sfMenu m')
    ->leftJoin('m.Permissions p');
    
    $tree = Doctrine_Core::getTable("sfMenu")->getTree();
    
    $tree->setBaseQuery($q);

    $tree = $tree->fetchBranch($this->item_id, array(), Doctrine_Core::HYDRATE_ARRAY_HIERARCHY);
    
    $tree = $tree[0]['__children'];
    
    if ($this->format == 'html') {

      $out = $this->template->getWrapper(array('item' => $this->getTemplatedTree($tree, 1)))."\n";
      
      $out = $this->template->clear($out);
      
    } else if ($this->format == 'json') {

      $out = json_encode($tree);
      
    } else if ($this->format == 'array' or !in_array($this->format, $this->formats)) {
      
      if($this->level){
    
         $out = $this->getTree($tree, 1);
            
      } else {
          
          $out = $tree; 
      }
      
    }

    return $out;
  }

  protected function getTemplatedTree($records, $level) {
    
    $out = '';
    
    $_level = $level;

    foreach ($records as $key => $record) {
      
      if(!$this->checkPermission($record['Permissions'])) continue;
      
      $options = array('url' => $this->url($record["url"]), 'title' => $record["title"]);
      
      if($record['is_new_window'] == true) $options = array_merge($options, array('target' => 'target="_blank"'));
      
      if($record['url'] == $this->current_path) $options = array_merge($options, array('active' => $this->template->getActive()));

      $item = $this->template->getItem($options);

      if (!empty($record['__children']) and (!$this->level or ($this->level and $level < $this->level))) {
        
        $tree = $this->template->getItemWrapper(array('item' => $this->getTemplatedTree($record['__children'], $_level++)));
        
        $item = $this->template->setVar($item, array('item_wrapper' => $tree));
        
      }
      
      $out .= $item."\n";
    }

    return $out;
  }
  
  protected function getTree($records, $level) {

    $i = 0;
    
    $_level = $level;
    
    foreach ($records as $key => $record) {
      
      if(!$this->checkPermission($record['Permissions'])) continue;
        
      $tree[$i] = $record; 
      
      if (!empty($record['__children'])){
          
        if(!$this->level or ($this->level and $level < $this->level)){
            
           $tree[$i]['__children'] = $this->getTree($record['__children'], $_level++);
            
        } else {
            
            $tree[$i]['__children'] = array();
        }
        
      }
      
      $i++;
      
    }

    return $tree;
  }
  
  protected function checkPermission($permissions){
    
    $is_checked = true;
    
    if(!empty($permissions)){
      
      $is_checked = false;
    
      foreach($permissions as $perm){
        
        if(sfContext::getInstance()->getUser()->hasCredential($perm['name'])){
          
          $is_checked = true;
          
          break;
          
        }
        
      }
      
    }
    
    return $is_checked;
  }
  
  protected function url($url){
    
    if(preg_match('/^@/', $url)){
      
      $url = sfContext::getInstance()->getRouting()->generate(substr($url, 1), array(), $this->absolute_url);
      
    } else if(!preg_match('/^[http|https]/', $url) and $this->path_prefix){
      
      $url = $this->path_prefix.$url;
   
    }
    
    return $url;
  }

  public function render($format = 'array') {
    
    $this->format = $format;
    
    $menu = $this->getMenu();

    return $menu;
  }

}
