<?php

class sfDoctrineMenuRouting extends sfPatternRouting {

  static public function listenToRoutingLoadConfigurationEvent(sfEvent $event) {

    $routing = $event->getSubject();

    $routing->prependRoute('sf_menu', new sfDoctrineRouteCollection(array(
      'name'                => 'sf_menu',
      'model'               => 'sfMenu',
      'module'              => 'sfMenu',
      'prefix_path'         => 'sf_menu',
      'with_wildcard_routes' => true,
      'requirements'        => array()
    )));
    
    $routing->prependRoute('sf_menu_items', new sfRoute('sf_menu/:menu_id/items', array('module' => 'sfMenuItem', 'action' => 'index'))); 
  }

}
