<?php

class sfMenuTemplate {
  
  private $template;

  public function __construct() {
    
    $this->template['active'] = 'active';
    
  }
  
  public function init(){
    
    return new sfMenuTemplate();
  }
  
  public function setWrapper($string){
    
    $this->template['wrapper'] = $string;
    
  }
  
  public function setVar($string, $values = null){
    
    if(is_array($values)){
      
      foreach($values as $key => $val){
        
        $temp_var['{{'.trim($key).'}}'] = $val;
      }
      
      $string = str_replace(array_keys($temp_var), $temp_var, $string);
      
    }
    
    return $string;
    
  }
  
  public function clear($string){
    
    return preg_replace("/{{(\w+)}}/", '', $string); 
    
  }
  
  public function getWrapper($values = null){
    
    $template = '';
    
    $template = $this->setVar($this->template['wrapper'], $values);
    
    return $template;
    
  }
  
  
  public function setItemWrapper($string){
    
    $this->template['item_wrapper'] = $string;
    
  }
  
  public function getItemWrapper($values = null){
    
    $template = '';
    
    $template = $this->setVar($this->template['item_wrapper'], $values);
    
    return $template;
    
  }
  
  public function setItem($string){
    
    $this->template['item'] = $string;
    
  }
  
  public function getItem($values = null){
    
    $template = '';
    
    $template = $this->setVar($this->template['item'], $values);
    
    return $template;
    
  }
  
  public function setActive($string){
    
    $this->template['active'] = $string;
    
  }
  
  public function getActive(){
    
    return $this->template['active'];
    
  }
  
  public function setFromArray($template){
    
    foreach($template as $key => $val){
      
      $this->template[$key] = $val;
      
    }
    
  }
  
  public function getTemplate(){
    
    return $this->template;
  }

}
