<?php

require_once dirname(__FILE__).'/../lib/sfMenuGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sfMenuGeneratorHelper.class.php';

/**
 * menu actions.
 *
 * @package    cms
 * @subpackage menu
 * @author     GrifiS
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfMenuActions extends autoSfMenuActions
{
  
  public function executeListItems(sfWebRequest $request)
  {
    $this->redirect('@sf_menu_items?menu_id='.$this->getRoute()->getObject()->getId());
  }
  
  protected function buildQuery() {

    $query = parent::buildQuery();

    $query->andWhere('level = ?', 0);

    return $query;
    
  }
  
}
