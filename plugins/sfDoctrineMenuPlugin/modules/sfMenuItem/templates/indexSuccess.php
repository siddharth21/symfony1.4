<?php use_stylesheet('/sfDoctrineMenuPlugin/js/themes/default/style.css') ?>
<?php use_stylesheet('/sfDoctrineMenuPlugin/js/fancybox/jquery.fancybox-1.3.4.css') ?>
<?php use_stylesheet('/sfDoctrineMenuPlugin/css/menu.css') ?>
<?php use_javascript('/sfDoctrineMenuPlugin/js/jquery.jstree.js') ?>
<?php use_javascript('/sfDoctrineMenuPlugin/js/fancybox/jquery.fancybox-1.3.4.pack.js') ?>
<?php use_javascript('/sfDoctrineMenuPlugin/js/jquery.form.js') ?>
<?php use_javascript('/sfDoctrineMenuPlugin/js/jquery.validate.min.js') ?>
<?php use_javascript('/sfDoctrineMenuPlugin/js/menu_item.js?v2') ?>
<div id="sf_admin_container">
  <div><h1><?php echo $title ?></h1></div>
  <div id="tree">
    <?php echo html_entity_decode($menu); ?>
  </div>

  <div style="margin-top: 15px"><input id="create_item" type="button" value="Создать новый пункт меню" /></div>
</div>
<script type="text/javascript">
  
  var parent;
  
  var root = <?php echo $root ?>;
  
  var url_get_form = "<?php echo url_for('sfMenuItem/getForm') ?>";
  
  var url_create = "<?php echo url_for('sfMenuItem/create') ?>";
  
  var url_edit = "<?php echo url_for('sfMenuItem/edit') ?>";
  
  var url_move = "<?php echo url_for('sfMenuItem/move') ?>";
  
  var url_remove = "<?php echo url_for('sfMenuItem/remove') ?>";

</script>