<script type="text/javascript">
  $(function () { 
    $("#item_form").validate({
      rules: {
        "sf_menu[title]": "required",
        "sf_menu[url]": "required"
      },
      messages: {
        "sf_menu[title]": "Обязательно",
        "sf_menu[url]": "Обязательно"
      }
    });
  });
</script>
<div class="ajax_form">
  <form id="item_form" action="<?php echo url_for('sfMenuItem/edit') ?>" method="post" >
    <?php foreach ($form as $field): ?>
      <?php if (!$field->isHidden()): ?>
        <div class="label"><?php echo $field->renderLabel() ?></div>
        <div class="field"><?php echo $field->render() ?></div>
      <?php endif ?>
    <?php endforeach ?>
    <div class="hidden_fields"><?php echo $form->renderHiddenFields() ?></div>
    <div class="submit">
      <input name="save" id="item_submit" type="submit" value="Сохранить" /><img id="form_loader" style="display: none" src="/sfDoctrineMenuPlugin/images/loader.gif" />
    </div>
  </form>
</div>