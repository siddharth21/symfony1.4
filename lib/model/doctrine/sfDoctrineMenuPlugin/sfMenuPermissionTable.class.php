<?php

/**
 * sfMenuPermissionTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class sfMenuPermissionTable extends PluginsfMenuPermissionTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object sfMenuPermissionTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('sfMenuPermission');
    }
}