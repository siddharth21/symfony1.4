<?php

/**
 * sfCategoryTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class sfCategoryTable extends PluginsfCategoryTable {

  /**
   * Returns an instance of this class.
   *
   * @return object sfCategoryTable
   */
  public static function getInstance() {
    return Doctrine_Core::getTable('sfCategory');
  }
  
  public function getCategories($root, $level = 1) {

    $q = $this->createQuery('a')
            ->where('a.root_id = ?', $root)
            ->andWhere('a.level >= ?', $level)
            ->orderBy('a.lft ASC');
    
    return $q;
  }
  
  public function getChildCategories($root_id) {

    $query = $this->createQuery('a')
            ->where('a.root_id = ?', $root_id)
            ->andWhere('a.level > ?', 0)
            ->orderBy('a.lft ASC');

    return $query;
  }

}