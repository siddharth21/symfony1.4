<?php

class sfCMSDoctrineFormGenerator extends sfDoctrineFormGenerator
{
  public function __construct(sfGeneratorManager $generatorManager)
  {
    $this->setTheme('cms');
    
    $this->initialize($generatorManager);
  }
  
  public function getWidgetClassForColumn($column)
  {
    switch ($column->getDoctrineType())
    {
      case 'string':
        $widgetSubclass = null === $column->getLength() || $column->getLength() > 255 ? 'Textarea' : 'InputText';
        break;
      case 'boolean':
        $widgetSubclass = 'InputCheckbox';
        break;
      case 'blob':
      case 'clob':
        $widgetSubclass = 'Textarea';
        break;
      case 'date':
        $widgetSubclass = 'Date';
        break;
      case 'time':
        $widgetSubclass = 'Time';
        break;
      case 'timestamp':
        $widgetSubclass = 'DateTime';
        break;
      case 'enum':
        $widgetSubclass = 'Choice';
        break;
      default:
        $widgetSubclass = 'InputText';
    }

    if ($column->isPrimaryKey())
    {
      $widgetSubclass = 'InputHidden';
    }
    else if ($column->isForeignKey())
    {
      $widgetSubclass = 'DoctrineChoice';
    }
    
    $widgetSubclass = $this->getExtraWidget($column, $widgetSubclass);

    return sprintf('sfWidgetForm%s', $widgetSubclass);
  }
  
  public function getValidatorClassForColumn($column)
  {
    switch ($column->getDoctrineType())
    {
      case 'boolean':
        $validatorSubclass = 'Boolean';
        break;
      case 'string':
    		if ($column->getDefinitionKey('email'))
    		{
    		  $validatorSubclass = 'Email';
    		}
    		else if ($column->getDefinitionKey('regexp'))
    		{
    		  $validatorSubclass = 'Regex';
    		}
    		else
    		{
    		  $validatorSubclass = 'String';
    		}
        break;
      case 'clob':
      case 'blob':
        $validatorSubclass = 'String';
        break;
      case 'float':
      case 'decimal':
        $validatorSubclass = 'Number';
        break;
      case 'integer':
        $validatorSubclass = 'Integer';
        break;
      case 'date':
        $validatorSubclass = 'Date';
        break;
      case 'time':
        $validatorSubclass = 'Time';
        break;
      case 'timestamp':
        $validatorSubclass = 'DateTime';
        break;
      case 'enum':
        $validatorSubclass = 'Choice';
        break;
      default:
        $validatorSubclass = 'Pass';
    }

    if ($column->isForeignKey())
    {
      $validatorSubclass = 'DoctrineChoice';
    }
    else if ($column->isPrimaryKey())
    {
      $validatorSubclass = 'Choice';
    }
    
    $validatorSubclass = $this->getExtraValidator($column, $validatorSubclass);

    return sprintf('sfValidator%s', $validatorSubclass);
  }
  
  public function getWidgetOptionsForColumn($column)
  {
    $options = array();

    if ($column->isForeignKey())
    {
      $options[] = sprintf('\'model\' => $this->getRelatedModelName(\'%s\'), \'add_empty\' => %s', $column->getRelationKey('alias'), $column->isNotNull() ? 'false' : 'true');
    }
    else if ('enum' == $column->getDoctrineType() && is_subclass_of($this->getWidgetClassForColumn($column), 'sfWidgetFormChoiceBase'))
    {
      $options[] = '\'choices\' => '.$this->arrayExport(array_combine($column['values'], $column['values']));
    }
    
    $options = $this->getExtraWidgetOptions($column, $options);

    return count($options) ? sprintf('array(%s)', implode(', ', $options)) : '';
  }
  
  protected function getExtraWidget($column, $widgetSubclass){
    
    $extra = $column->getDefinitionKey('extra');
    
    if($extra){
      
      if($widgetSubclass == 'Textarea'){
      
        if($extra['type'] == 'editor'){
          $widgetSubclass = 'Tinymce';
        }
        
      }
     
      if($extra['type'] == 'image'){
         $widgetSubclass = 'InputImageCache';
      }
      
    }
    
    
    return $widgetSubclass;
  }
  
  protected function getExtraValidator($column, $validatorSubclass){
    
    $extra = $column->getDefinitionKey('extra');
    
    if($extra){
     
      if($extra['type'] == 'image'){
         $validatorSubclass = 'File';
      }
      
    }
    
    return $validatorSubclass;
  }
  
  
  protected function getExtraWidgetOptions($column, $options){
    
    $extra = $column->getDefinitionKey('extra');
    
    if($extra){
      
      if($extra['type'] == 'editor'){
        
        foreach($extra as $key => $val){
          
          if($key == 'type') continue;
          
          if(is_string($val)) $val = '\''.$val.'\'';
          
          $options[] = '\''.$key.'\' => '.$val;
          
        }
      }
      
      if($extra['type'] == 'image'){

        $options[] = '\'image\' => $this->getObject()->get(\''.$column->getName().'\') ? $this->getObject()->get(\''.$column->getName().'\') : \'\'';
        
        if(isset($extra['imageCacheFormat'])){
          $options[] = '\'image_cache_format\' => \''.$extra['imageCacheFormat'].'\'';
        } else {
          $options[] = '\'image_cache_format\' => \'default\'';
        }

        if(isset($extra['path'])){
          $options[] = '\'path\' => \''.$extra['path'].'\'';
        } else {
          $options[] = '\'path\' => \'images\'';
        }
        
        if(isset($extra['withDelete'])){
          $boolen = $extra['withDelete'] ? 'true' : 'false';
          $options[] = '\'with_delete\' => '.$boolen;
        } else {
          $options[] = '\'with_delete\' => true';
        }
        

        $options[] = '\'edit_mode\' => $this->getObject()->get(\''.$column->getName().'\') ? true : false';
        
      }
      
    }
    
    return $options;
  }
  
  public function getValidatorOptionsForColumn($column)
  {
    $options = array();

    if ($column->isForeignKey())
    {
      $options[] = sprintf('\'model\' => $this->getRelatedModelName(\'%s\')', $column->getRelationKey('alias'));
    }
    else if ($column->isPrimaryKey())
    {
      $options[] = sprintf('\'choices\' => array($this->getObject()->get(\'%s\')), \'empty_value\' => $this->getObject()->get(\'%1$s\')', $column->getFieldName());
    }
    else
    {
      switch ($column->getDoctrineType())
      {
        case 'string':
          if ($column['length'])
          {
            $options[] = sprintf('\'max_length\' => %s', $column['length']);
          }
          if (isset($column['minlength']))
          {
            $options[] = sprintf('\'min_length\' => %s', $column['minlength']);
          }
          if (isset($column['regexp']))
          {
            $options[] = sprintf('\'pattern\' => \'%s\'', $column['regexp']);
          }
          break;
        case 'enum':
          $options[] = '\'choices\' => '.$this->arrayExport($column['values']);
          break;
      }
    }

    // If notnull = false, is a primary or the column has a default value then
    // make the widget not required
    if (!$column->isNotNull() || $column->isPrimaryKey() || $column->hasDefinitionKey('default'))
    {
      $options[] = '\'required\' => false';
    }
    
    $options = $this->getExtraValidatorOptions($column, $options);

    return count($options) ? sprintf('array(%s)', implode(', ', $options)) : '';
  }
  
  protected function getExtraValidatorOptions($column, $options){
    
    $extra = $column->getDefinitionKey('extra');
    
    if($extra){
      
      if($extra['type'] == 'image'){
        
        unset($extra['type']);
        
        if($extra['imageCacheFormat']){
          
          $options[] = '\'path\' => sfConfig::get(\'sf_upload_dir\').\'/'.$extra['path'].'/\'';
          
        } else {
          
          $options[] = '\'path\' => sfConfig::get(\'sf_upload_dir\').\'/'.$extra['path'].'/\'';
          
        }
        
        $options[] = '\'mime_types\' => \'web_images\'';
        
        $options[] = '\'validated_file_class\' => \'sfValidatedFileSeo\'';
        
        foreach($options as $key => $option){
        
          if(preg_match('/max_length/', $option) or preg_match('/min_length/', $option)){
          
            unset($options[$key]);
          }
        }
        
      }
      
    }
    
    return $options;
  }
  
  public function checkExtraType($column, $type = false){
    
    $extra = $column->getDefinitionKey('extra');
    
    if($type and $extra){
      
      if($extra['type'] == $type) return true;
      
    }
    
    return false;
    
  }
  
}
