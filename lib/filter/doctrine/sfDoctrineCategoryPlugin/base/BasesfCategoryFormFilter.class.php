<?php

/**
 * sfCategory filter form base class.
 *
 * @package    cms
 * @subpackage filter
 * @author     GrifiS
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasesfCategoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'title'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'slug'            => new sfWidgetFormFilterInput(),
      'seo_description' => new sfWidgetFormFilterInput(),
      'seo_keywords'    => new sfWidgetFormFilterInput(),
      'root_id'         => new sfWidgetFormFilterInput(),
      'lft'             => new sfWidgetFormFilterInput(),
      'rgt'             => new sfWidgetFormFilterInput(),
      'level'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'title'           => new sfValidatorPass(array('required' => false)),
      'slug'            => new sfValidatorPass(array('required' => false)),
      'seo_description' => new sfValidatorPass(array('required' => false)),
      'seo_keywords'    => new sfValidatorPass(array('required' => false)),
      'root_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'lft'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'rgt'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'level'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('sf_category_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfCategory';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'title'           => 'Text',
      'slug'            => 'Text',
      'seo_description' => 'Text',
      'seo_keywords'    => 'Text',
      'root_id'         => 'Number',
      'lft'             => 'Number',
      'rgt'             => 'Number',
      'level'           => 'Number',
    );
  }
}
