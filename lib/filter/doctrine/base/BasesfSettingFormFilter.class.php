<?php

/**
 * sfSetting filter form base class.
 *
 * @package    cms
 * @subpackage filter
 * @author     GrifiS
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasesfSettingFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sysname' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'value'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'sysname' => new sfValidatorPass(array('required' => false)),
      'value'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sf_setting_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfSetting';
  }

  public function getFields()
  {
    return array(
      'id'      => 'Number',
      'sysname' => 'Text',
      'value'   => 'Text',
    );
  }
}
