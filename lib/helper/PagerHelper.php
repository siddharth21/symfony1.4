<?php

function url_for_pager($url, $page) {

  $query_string = $_SERVER['QUERY_STRING'];

  if(!empty($query_string)) {

    $q = explode('&', $query_string);

    foreach ($q as $key => $val) {

      if (preg_match('/^page=/', $val))
        unset($q[$key]);
    }
  }

  $_page = 'page=' . $page;
  
  $q[] = $_page;


  if (count($q) > 1) {

    $query_string = implode('&', $q);
    
  } else {

    $query_string = $_page;
  }



  return url_for($url) . '?' . $query_string;
}
