<?php

/**
 * sfNews form base class.
 *
 * @method sfNews getObject() Returns the current form's model object
 *
 * @package    cms
 * @subpackage form
 * @author     GrifiS
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasesfNewsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'title'           => new sfWidgetFormInputText(),
      'body'            => new sfWidgetFormTinymce(array('format' => 'default')),
      'image'           => new sfWidgetFormInputImageCache(array('image' => $this->getObject()->get('image') ? $this->getObject()->get('image') : '', 'image_cache_format' => 'default', 'path' => 'images/news', 'with_delete' => true, 'edit_mode' => $this->getObject()->get('image') ? true : false)),
      'image2'          => new sfWidgetFormInputImageCache(array('image' => $this->getObject()->get('image2') ? $this->getObject()->get('image2') : '', 'image_cache_format' => 'default', 'path' => 'images/news', 'with_delete' => true, 'edit_mode' => $this->getObject()->get('image2') ? true : false)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'slug'            => new sfWidgetFormInputText(),
      'seo_description' => new sfWidgetFormTextarea(),
      'seo_keywords'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'           => new sfValidatorString(array('max_length' => 255)),
      'body'            => new sfValidatorString(array('required' => false)),
      'image'           => new sfValidatorFile(array('required' => false, 'path' => sfConfig::get('sf_upload_dir').'/images/news/', 'mime_types' => 'web_images', 'validated_file_class' => 'sfValidatedFileSeo')),
      'image_delete'    => new sfValidatorPass(),
      'image2'          => new sfValidatorFile(array('required' => false, 'path' => sfConfig::get('sf_upload_dir').'/images/news/', 'mime_types' => 'web_images', 'validated_file_class' => 'sfValidatedFileSeo')),
      'image2_delete'    => new sfValidatorPass(),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'slug'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'seo_description' => new sfValidatorString(array('required' => false)),
      'seo_keywords'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sf_news[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfNews';
  }

}
