<?php

/**
 * sfCategory form base class.
 *
 * @method sfCategory getObject() Returns the current form's model object
 *
 * @package    cms
 * @subpackage form
 * @author     GrifiS
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasesfCategoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'title'           => new sfWidgetFormInputText(),
      'slug'            => new sfWidgetFormInputText(),
      'seo_description' => new sfWidgetFormTextarea(),
      'seo_keywords'    => new sfWidgetFormInputText(),
      'root_id'         => new sfWidgetFormInputText(),
      'lft'             => new sfWidgetFormInputText(),
      'rgt'             => new sfWidgetFormInputText(),
      'level'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'           => new sfValidatorString(array('max_length' => 255)),
      'slug'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'seo_description' => new sfValidatorString(array('required' => false)),
      'seo_keywords'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'root_id'         => new sfValidatorInteger(array('required' => false)),
      'lft'             => new sfValidatorInteger(array('required' => false)),
      'rgt'             => new sfValidatorInteger(array('required' => false)),
      'level'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sf_category[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfCategory';
  }

}
