<?php

/**
 * sfNews form.
 *
 * @package    cms
 * @subpackage form
 * @author     GrifiS
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfNewsForm extends BasesfNewsForm
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at']);
  }
}
