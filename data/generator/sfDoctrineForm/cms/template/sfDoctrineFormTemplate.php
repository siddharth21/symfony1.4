[?php

/**
 * <?php echo $this->table->getOption('name') ?> form.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class <?php echo $this->table->getOption('name') ?>Form extends Base<?php echo $this->table->getOption('name') ?>Form
{
<?php if ($parent = $this->getParentModel()): ?>
  /**
   * @see <?php echo $parent ?>Form
   */
  public function configure()
  {
    parent::configure();
    
<?php if($this->table->hasTemplate('Timestampable')): ?>
    unset($this['created_at'], $this['updated_at]);
<?php endif ?> 
  }
<?php else: ?>
  public function configure()
  {
<?php if($this->table->hasTemplate('Timestampable')): ?>
    unset($this['created_at'], $this['updated_at']);
<?php endif ?>
  }
<?php endif; ?>
}
