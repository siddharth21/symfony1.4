[?php

/**
 * Project form base class.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: sfDoctrineFormBaseTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class BaseFormDoctrine extends sfFormDoctrine
{
  public function setup()
  {
  }
  
  public function saveFile($field, $filename = null, sfValidatedFile $file = null) {
    
    $values = $this->getTaintedValues();
    
    $definition = $this->getObject()->getTable()->getColumnDefinition($field);
    
    $extra = $definition['extra'];
    
    if($extra){
      
      if($extra['type'] == 'image'){
        
        if(preg_match('/^_/', $extra['fileName'])){
          
          $filename = $values[substr($extra['fileName'], 1)];
          
        } else {
          
          $filename = $extra['fileName'];
          
        }
        
      }
      
    }
    
    return parent::saveFile($field, $filename, $file);
  }
  
  public function removeFile($field) {
    
    parent::removeFile($field);
    
    $this->clearCache($field);
  }

  protected function clearCache($field){

    if(!empty($this->getObject()->$field)){

      $definition = $this->getObject()->getTable()->getColumnDefinition($field);

      $extra = $definition['extra'];

      $path = $extra['path'];

      $_formats = sfConfig::get('imagecache_formats');

      foreach($_formats as $format => $options){

        $file = sfConfig::get('sf_upload_dir') . '/cache/' . trim($format).'/'.$path.'/'.$this->getObject()->$field;

        unlink($file);
      }
    }
  }
}
